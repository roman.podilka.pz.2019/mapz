class AbstractFactory
  # @abstract
  def sport_task
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  # @abstract
  def education_task
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class HomeTaskFactory < AbstractFactory
  def sport_task
    SportHomeTask.new
  end

  def education_task
    EducationHomeTask.new
  end
end

class OutsideTaskFactory < AbstractFactory
  def sport_task
    SportOutsideTask.new
  end

  def education_task
    EducationOutsideTask.new
  end
end

# abstract home task
class AbstractHomeTask
  def return_success
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class SportHomeTask < AbstractHomeTask
  def return_success
    puts "Hurray! You successfully created sport home task!"
  end
end

class EducationHomeTask < AbstractHomeTask
  def return_success
    puts "Hurray! You successfully created education home task!"
  end
end

# abstract outside task
class AbstractOutsideTask
  def return_success
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class SportOutsideTask < AbstractOutsideTask
  def return_success
    puts "Hurray! You successfully created sport outside task!"
  end
end

class EducationOutsideTask < AbstractOutsideTask
  def return_success
    puts "Hurray! You successfully created education outside task!"
  end
end

def home_factory
  home = HomeTaskFactory.new
  sport = home.sport_task
  education = home.education_task
  puts sport.return_success
  puts education.return_success
end

def outside_factory
  outside = OutsideTaskFactory.new
  sport = outside.sport_task
  education = outside.education_task
  puts sport.return_success
  puts education.return_success
end

home_factory
outside_factory