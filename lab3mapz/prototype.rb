class Task
  attr_accessor :title, :date, :tag, :circular_reference

  def initialize
    @title = nil
    @date = nil
    @tag = nil
    @circular_reference = nil
  end

  def clone
    @date = deep_copy(@date)
    @tag = deep_copy(@tag)
    @circular_reference = deep_copy(@circular_reference)
    @circular_reference.prototype = self
    deep_copy(self)
  end

  private
  def deep_copy(object)
    Marshal.load(Marshal.dump(object))
  end
end

class TaskId
  attr_accessor :prototype, :id

  def initialize(prototype, id)
    @prototype = prototype
    @id = id
  end
end

p1 = Task.new
p1.title = "Fix json error"
p1.date = Time.now
p1.tag = "Fix"
p1.circular_reference = TaskId.new(p1, 15)

p2 = p1.clone

if p1.title == p2.title
  puts 'Title field values have been carried over to a clone. Yay!'
else
  puts 'Title field values have not been copied. Booo!'
end

if p1.date.equal?(p2.date)
  puts 'Simple date has not been cloned. Booo!'
else
  puts 'Simple date has been cloned. Yay!'
end

if p1.circular_reference.equal?(p2.circular_reference)
  puts 'Component with back reference has not been cloned. Booo!'
else
  puts 'Component with back reference has been cloned. Yay!'
end

if p1.circular_reference.prototype.equal?(p2.circular_reference.prototype)
  puts 'Component with back reference is linked to original object. Booo!'
else
  puts 'Component with back reference is linked to the clone. Yay!'
end

if p1.circular_reference.id.equal?(p2.circular_reference.id)
  puts 'Id was copied!'
else
  puts 'Id was not copied'
end
