# singleton for family plan in our planning app
class SingletonFamilyPlan
  # some kind of DB
  attr_accessor :family_tasks

  def initialize
    self.family_tasks = ['Wash a dog', 'Drink water', 'Do sport', 'Be happy']
  end

  @instance = new

  def self.instance
    @instance
  end

  def show_tasks
    puts "Your family has this number of tasks #{family_tasks.count}:"
    family_tasks.each do |task|
      puts "- #{task}"
    end
    puts
  end
end

s1 = SingletonFamilyPlan.instance
s2 = SingletonFamilyPlan.instance

s1.show_tasks
s2.family_tasks[2] = 'Relax'
s1.show_tasks
