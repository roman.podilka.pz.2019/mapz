class Subscription
  attr_accessor :state
  private :state

  def initialize(state)
    puts 'Init'
    transition_to(state)
    puts
  end

  def transition_to(state)
    puts "Subscription: Transition to #{state.class}"
    @state = state
    @state.subscription = self
  end

  def start
    @state.pay_subscription
  end

  def end
    @state.end_subscription
  end
end

class State
  attr_accessor :subscription

  def pay_subscription
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def end_subscription
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class EndedSubscription < State
  def pay_subscription
    puts 'EndedSubscription handles start.'
    puts 'EndedSubscription wants to change the state of the subscription(to PayedSubscription).'
    @subscription.transition_to(PayedSubscription.new)
    puts
  end

  def end_subscription
    puts 'EndedSubscription handles end.'
  end
end

class PayedSubscription < State
  def pay_subscription
    puts 'PayedSubscription handles start.'
  end

  def end_subscription
    puts 'PayedSubscription handles end.'
    puts 'PayedSubscription wants to change the state of the subscription(to EndedSubscription).'
    @subscription.transition_to(EndedSubscription.new)
    puts
  end
end

context = Subscription.new(EndedSubscription.new)

context.start
context.end
context.end
