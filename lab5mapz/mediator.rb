class Mediator
  def notify(_sender, _event)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class ConcreteMediator < Mediator
  def initialize(component1, component2, component3)
    @component1 = component1
    @component1.mediator = self
    @component2 = component2
    @component2.mediator = self
    @component3 = component3
    @component3.mediator = self
  end

  # @param [Object] sender
  # @param [String] event
  def notify(_sender, event)
    if event == 'post'
      puts 'Mediator reacts on creating post and triggers following operations:'
      @component3.check_access
      @component1.add_tags
      @component2.set_status
      @component3.validate
    elsif event == 'delete task'
      puts 'Mediator reacts on delete task and triggers following operations:'
      @component3.check_access
      @component3.password_pop_up
      @component3.confirmation
      @component2.delete_db_connections
    end
  end
end

class BaseComponent
  # @return [Mediator]
  attr_accessor :mediator

  # @param [Mediator] mediator
  def initialize(mediator = nil)
    @mediator = mediator
  end
end

class Validation < BaseComponent
  def validate
    puts 'Validation: Current info successfully passed the validation!'
    @mediator.notify(self, 'validate')
  end

  def check_access
    puts 'Validation: Current user has rights to do this!'
    @mediator.notify(self, 'check user access')
  end

  def confirmation
    puts 'Validation: Are you serious? Hmm...okay'
    @mediator.notify(self, 'confirm')
  end

  def password_pop_up
    puts 'Validation: Please fill this annoying pop up'
    @mediator.notify(self, 'pop up')
  end
end

class Post < BaseComponent
  def post
    @mediator.notify(self, 'post')
    puts 'Post: Current user has successfully created post!'
  end

  def add_tags
    puts 'Post: Added tags.'
    @mediator.notify(self, 'tag')
  end
end

class Task < BaseComponent
  def delete
    @mediator.notify(self, 'delete task')
    puts 'Task: Current task has been deleted!'
  end

  def delete_db_connections
    puts 'Task: DB connections have been removed'
    @mediator.notify(self, 'connections')
  end

  def set_status
    puts 'Task: Status has been set'
    @mediator.notify(self, 'status')
  end
end

new_post = Post.new
task = Task.new
validation = Validation.new

ConcreteMediator.new(new_post, task, validation)

task.delete
puts
new_post.post
