class SubscriptionHandler
  def next_handler=(handler)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def handle(request)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class Subscription < SubscriptionHandler
  attr_writer :next_handler

  def next_handler(handler)
    @next_handler = handler
    handler
  end

  def handle(request)
    return @next_handler.handle(request) if @next_handler

    'Nothing happened'
  end
end

class EndedSubscription < Subscription
  def handle(request)
    if request == 'Buy'
      "EndedSubscription changed status to: #{request}"
    else
      super(request)
    end
  end
end

class ActiveSubscription < Subscription
  def handle(request)
    if request == 'Stop'
      "ActiveSubscription changed status to: #{request}"
    else
      super(request)
    end
  end
end

class ExpiredSubscription < Subscription
  def handle(request)
    if request == 'Renew'
      "ExpiredSubscription changed status to: #{request}"
    else
      super(request)
    end
  end
end

ended_sub = EndedSubscription.new
active_sub = ActiveSubscription.new
expired_sub = ExpiredSubscription.new

ended_sub.next_handler(active_sub).next_handler(expired_sub)

puts 'Chain: Ended > Active > Expired'

['Renew', 'Buy', 'Stop'].each do |status|
  puts "Now is #{status}"
  res = ended_sub.handle(status)
  if res
    puts "#{res}"
  else
    puts "#{status} was not implemented in our system"
  end
end
