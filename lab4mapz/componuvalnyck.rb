class UserPlan
  attr_accessor :name
  def initialize
    @title = title
  end
  def add(plan)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def remove(plan)
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def composite?
    false
  end

  def leaf?
    false
  end

  def sum
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

# composite class
class Subscription < UserPlan
  attr_accessor :plans
  def initialize(name)
    @plans = []
    @name = name
  end

  def add(plan)
    @plans.append(plan)
  end

  def remove(plan)
    @plans.remove(plan)
  end

  def composite?
    true
  end

  def sum
    suma = 0
    @plans.each { |plan| suma += plan.sum }
    suma
  end
end

# leaf
class UserInfo < UserPlan
  attr_accessor :name, :time

  def initialize(name, time)
    @name = name
    @time = time
  end

  def sum
    time
  end
end

subscription = Subscription.new('Self development')

plan1 = Subscription.new('Petro Mostavchuk')
plan1.add(UserInfo.new('Philosophy', 70))
plan1.add(UserInfo.new('Sport', 90))

plan2 = Subscription.new('Ihor Voitenko')
plan2.add(UserInfo.new('Philosophy', 20))
plan2.add(UserInfo.new('Sport', 50))

subscription.add(plan1)
subscription.add(plan2)

puts subscription.sum

hours = subscription.sum / 60
minutes = subscription.sum % 60

puts "You spend around #{hours}:#{minutes} hours on your subscriptions"
