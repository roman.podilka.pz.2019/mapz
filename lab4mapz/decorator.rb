class Notifier
  attr_accessor :user, :task
  def initialize(user, task)
    @user = user
    @task = task
  end

  def get_notification
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def set_notification_time
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end

  def set_notification_time_interval
    raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
  end
end

class NotifierDecorator < Notifier
  def initialize(notifier)
    @notifier = notifier
  end

  def get_notification
    @notifier.get_notification
  end

  def set_notification_time
    @notifier.set_notification_time
  end

  def set_notification_time_interval
    @notifier.set_notification_time_interval
  end
end

class SlackNotifier < NotifierDecorator
  attr_accessor :user, :task

  def get_notification
    "#{@notifier.user} has got notification from Slack"
  end

  def set_notification_time
    "#{@notifier.user} set notification time for Slack for his task:#{@notifier.task} at 15:00"
  end

  def set_notification_time_interval
    "#{@notifier.user} set Slack notification time interval from 10:00 to 18:00"
  end
end

class FacebookNotifier < NotifierDecorator
  attr_accessor :user, :task

  def get_notification
    "#{@notifier.user} has got notification from Facebook"
  end

  def set_notification_time
    "#{@notifier.user} set notification time for Facebook for his task:#{@notifier.task} at 20:00"
  end

  def set_notification_time_interval
    "#{@notifier.user} set Facebook notification time interval from 9:00 to 19:00"
  end
end

class GoogleNotifier < NotifierDecorator
  def get_notification
    "#{@notifier.user} has got notification from Google"
  end

  def set_notification_time
    "#{@notifier.user} set notification time for Google for his task:#{@notifier.task} at 12:00"
  end

  def set_notification_time_interval
    "#{@notifier.user} set Google notification time interval from 11:00 to 12:00"
  end
end

notifier = Notifier.new('Roman', 'Sport')

slack_notifier = SlackNotifier.new(notifier)

puts slack_notifier.get_notification
puts slack_notifier.set_notification_time
puts slack_notifier.set_notification_time_interval
puts

facebook_notifier = FacebookNotifier.new(notifier)

puts facebook_notifier.get_notification
puts facebook_notifier.set_notification_time
puts facebook_notifier.set_notification_time_interval
puts

google_notifier = GoogleNotifier.new(notifier)

puts google_notifier.get_notification
puts google_notifier.set_notification_time
puts google_notifier.set_notification_time_interval
