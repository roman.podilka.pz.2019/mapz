class Facade
  def initialize(user, task)
    @user = user || User.new
    @task = task || Task.new
  end

  def start
    results = []
    results.append('Facade initializes user, task')
    results.append('Connecting task with user')
    results.append('Show  results:')
    results.append(@user.show_info)
    results.append(@task.show_info)
    results.append('Facade has finished')
    results.join("\n")
  end
end

class User
  attr_accessor :id, :name, :plan

  def initialize(id, name, plan)
    @id = id
    @name = name
    @plan = plan
  end

  def show_info
    "User id:#{@id}, name:#{@name}, plan:#{@plan}"
  end
end

class Task
  attr_accessor :id, :title, :type

  def initialize(id, title, type)
    @id = id
    @title = title
    @type = type
  end

  def show_info
    task = prepare_task(@type)
    task + "Task id:#{@id}, title:#{@title}, type:#{@type}"
  end

  private

  def prepare_task(type)
    if type == 'to do'
      "Your task has been set for today at 15:00. Info:"
    else
      "Your task has been set for tomorrow. Info:"
    end
  end
end

user = User.new(1, 'Mark', 'Single')
task = Task.new(7, 'Walk for a 10 minutes', 'to do')

facade = Facade.new(user, task)

puts facade.start
